### Markdown Editor ###

 To run this app, you must have [node js](https://nodejs.org/en/download/) and [meteor](https://www.meteor.com/install) installed.
 Once you're good to go, clone this repo, switch to the correspondent folder and type:
 
`npm install`, and then `meteor`

 Then you can open your browser and go to localhost:3000 to see it.
 
 ### What is this repository for? ###
 
 * This is an app that allows a user to create, edit and modify documents written in MarkDown. The user can visualize the resulting HTML while editing a given document.
 * Version1.0