import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import ReactDOM from 'react-dom';

import { Documents } from '../api/documents.js';
import Document from './Document.jsx';

const {List, ListItem} = require('cf-component-list');

import { Link } from "react-router";

class Home extends Component {

     renderDocuments() {
       return this.props.documents.map((document) => (
         <Document key={document._id} document={document} />
       ));
     }

     createEmptyDocument() {
      Documents.insert({
        title: 'Untitled',
        text: '',
        createdAt: new Date(), // current time
        owner: Meteor.userId(),           // _id of logged in user
      });
     }

     render() {

      var spacerStyle = {
          marginTop: 50
      };
      var textCenter = {
        textAlign: "center"
      };

       return (
        <div className="container" style={textCenter}>

        { this.props.currentUser ?

            <div style={spacerStyle}>

              <header>
               <h2>Your documents</h2>
              </header>

              <button onClick={this.createEmptyDocument.bind(this)}>Add new document</button>
              <div style={spacerStyle}></div>
              <List unstyled>
               {this.renderDocuments()}
              </List> 
            </div>

         : <h2 style={textCenter}>Please sign in to use the editor!</h2>
        }

        </div>
       );
     }

}

Home.propTypes = {
  documents: PropTypes.array.isRequired,
  currentUser: PropTypes.object
};

export default createContainer(() => {
  return {
    documents: Documents.find({owner: Meteor.userId()}).fetch(),
    currentUser: Meteor.user()
  };
}, Home);