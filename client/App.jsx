import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import CodeMirror from 'react-codemirror'

import { createContainer } from 'meteor/react-meteor-data';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

import { Documents } from '../api/documents.js';
import Document from './Document.jsx';

import { Link } from "react-router";

const {LayoutContainer, LayoutRow, LayoutColumn} = require('cf-component-layout');
const {Heading} = require('cf-component-heading');

class App extends Component {

   render() {

    var spacerStyle = {
        marginTop: 20
    };

    var whiteBackground = {
        backgroundColor: "#ddd",
        borderRadius: 10,
        textAlign: "center"
    };

    var loginStyle = {
        marginRight: 100,
        textAlign: "right"
    };

      return (
        <div>
            <LayoutContainer>
                <div style={whiteBackground}>
                    <Heading size={2}>
                        MARKDOWN EDITOR
                    </Heading>
                </div>
                <div style={loginStyle}>
                    <AccountsUIWrapper style={whiteBackground} />
                </div>
                <div>
                    {this.props.children}
                </div>
            </LayoutContainer>
        </div>
      );
   }

}

export default App;