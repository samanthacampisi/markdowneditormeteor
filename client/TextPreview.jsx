import React, { Component } from 'react';

var Remarkable = require('remarkable');
var md = new Remarkable();

class TextPreview extends Component {
	render() {
	    var text = md.render(this.props.text);
	    return <div dangerouslySetInnerHTML={{__html: text}}></div>;		
	}
}