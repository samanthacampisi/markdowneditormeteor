import 'babel-polyfill'
import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import ProseMirror from 'react-prosemirror'
import './accounts-config.js';

import {Router, Route, IndexRoute, hashHistory} from 'react-router';

import App from './App.jsx';
import Home from './Home.jsx';
import Editor from './Editor.jsx';

const router = (
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home}></IndexRoute>
      <Route path="editor/:documentId" name="editor" component={Editor}></Route>
    </Route>
  </Router>
)

Meteor.startup(() => {
  render(router, document.getElementById('app'));
});
