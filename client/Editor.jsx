import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Link } from "react-router";

import {Flex, FlexItem} from 'cf-component-flex';
import {LayoutContainer, LayoutRow, LayoutColumn} from 'cf-component-layout';
import CodeMirror from 'react-codemirror'
import ProseMirror from 'react-prosemirror'
import 'prosemirror/dist/menu/menubar'
import 'prosemirror/dist/menu/tooltipmenu'
import 'prosemirror/dist/menu/menu'
import 'prosemirror/dist/markdown'

import { Documents } from '../api/documents.js';
import Document from './Document.jsx';



require('codemirror/mode/markdown/markdown');

var Remarkable = require('remarkable');
var md = new Remarkable();

export class Editor extends Component {

  constructor(props) {
    super(props);
  }

  updateTitle(documentId, event) {
    console.log('this is updateTitle');

    Documents.update(documentId, {$set: {title: event.target.value}});
  }

  updateText(documentId, text) {
    console.log('this is updateText');

    Documents.update(documentId, {$set: {text: text}});
  }

  render() {
    const { params } = this.props;
    const { documentId } = params;

    var convertedText = md.render(this.props.document.text);

    var spacerStyle = {
        marginTop: 20
    };
    var textCenter = {
      textAlign: "center"
    };
    var editorStyle = {
      borderColor: "#cccccc",
      border: 2
    };

    return (
      <div>

        <Link to="/"><button>Back</button></Link>

        { Meteor.userId() ? 
          <div style={spacerStyle}>
            { this.props.document ?
              <div>
                <LayoutContainer>
                  <LayoutRow>
                    Title: <input type="text" value={this.props.document.title} onChange={this.updateTitle.bind(this, documentId)} />
                    <div style={spacerStyle}></div>
                  </LayoutRow>
                  <LayoutRow>
                    <div style={spacerStyle}></div>
                    <div>
                      <div style={editorStyle}>
                        <LayoutColumn width={1}>
                          <ProseMirror value={this.props.document.text} onChange={this.updateText.bind(this, documentId)} options={{docFormat: 'markdown', menuBar: true,
        tooltipMenu: true}} />
                        </LayoutColumn>
                      </div>
                    </div>
                  </LayoutRow>
                </LayoutContainer>
              </div>
            : ''
            }
          </div>
        : <h2 style={textCenter}>Please sign in to use the editor!</h2>
        }

      </div>
    );
  }
}

Editor.propTypes = {
  document: PropTypes.object,
  currentUser: PropTypes.object
};

export default createContainer((param) => {
  const { params } = param;
  const { documentId } = params;
  console.log('editing document:', documentId);

  var initial = {text: '', title: ''};

  return {
    document: Documents.findOne({_id: documentId}) || initial,
    currentUser: Meteor.user()
   };
}, Editor);