import React, { Component, PropTypes } from 'react';
import { Link } from "react-router";
const {ListItem} = require('cf-component-list');

export default class Document extends Component {
  render() {
  	var linkStyle = {
  		textDecoration: "none",
  		fontSize: 18
  	};

    return (
      <Link to={`/editor/${this.props.document._id}`} style={linkStyle}><ListItem>{this.props.document.title}</ListItem></Link>
    );
  }
}

Document.propTypes = {
  document: PropTypes.object.isRequired,
};